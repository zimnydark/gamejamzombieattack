﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMaker : MonoBehaviour
{
    public Image m_HpBar;
    public Text m_Score;
    public static float m_FenceHp = 10000.0f;
    public float maxHP= 10000.0f;
    public GameObject m_GameOver;
    public Button m_PlayAgain;

    public Player m_Player;

    float timer = 0;
    float timerShow = 0;

    public List<Builder> objectSpawner = new List<Builder>();
    public List<Builder> MobSpawner = new List<Builder>();

    void Update()
    {
        m_HpBar.fillAmount = m_FenceHp / maxHP;
        timer+= Time.deltaTime;
        timerShow += Time.deltaTime;
        m_Score.text = timerShow.ToString();

        for (int i = 0; i < objectSpawner.Count; i++)
            {
                if (objectSpawner[i].Timer<=timer)
                    {
                        objectSpawner[i].myobject.SetActive(true);
                    }
             }

        for (int i = 0; i < MobSpawner.Count; i++)
        {
            if (MobSpawner[i].Timer <= timer)
            {

                MobSpawner[i].myobject.SetActive(true);
            }
            if(MobSpawner[i].Timer+10f <= timer)
            {
                MobSpawner[i].myobject.SetActive(false);
            }
        }
        if(timer>=135)
        {
            timer = 0;
        }

        if (m_Player.IsDead() || m_FenceHp <= 0)
        {
            m_GameOver.SetActive(true);
            m_PlayAgain.onClick.AddListener(()=>LoadScene());
  
        }
    }
    void LoadScene()
    {
        SceneManager.LoadScene("scena1");
    }
}

