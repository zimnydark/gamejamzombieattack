﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    public Transform m_Player;

    void LateUpdate()
    {
        Vector3 m_Position = m_Player.position;
        m_Position.y = transform.position.y;
        transform.position = m_Position;
    }
}
