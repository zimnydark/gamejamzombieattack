﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class Npc : MonoBehaviour
{
    public float m_MaxHp;
    public float m_HpRegen;
    public float m_MovementSpeed;
    public float m_Armour;
    public float m_Damage;
    public float m_AttackRange;

    float m_Hp;

    protected Animator m_Animator;
    public bool m_IsItMob = true;
    public LayerMask m_EnemyCheck;
    public Image m_HpImage;
    public AudioClip m_HitSound;
    public AudioClip m_DieSound;
    AudioSource m_AudioSource;
    

    public virtual void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
        m_Animator = GetComponent<Animator>();
        m_Hp = m_MaxHp;
    }

    public virtual void FixedUpdate()
    {
        
    }

    public void Attack()
    {
        Collider[] enemiesHit = Physics.OverlapSphere(transform.position, m_AttackRange, m_EnemyCheck);
        bool fenceHit = false;
        for (int i = 0; i < enemiesHit.Length; i++)
        {
            Npc npcHit = enemiesHit[i].GetComponent<Npc>();
            if (npcHit != null)
            {
                if (npcHit.m_Hp > 0)
                    npcHit.TakeDamage(m_Damage);
            }
            else if(!fenceHit)
            {
                GameMaker.m_FenceHp = GameMaker.m_FenceHp - m_Damage;
                fenceHit = true;
            }
        }
    }

    void TakeDamage(float damageTaken)
    {
        m_AudioSource.PlayOneShot(m_HitSound);
        m_Hp = m_Hp - damageTaken / (m_Armour / 100.0f);
        if (m_Hp <= 0f)
        {
            m_AudioSource.PlayOneShot(m_DieSound);
            m_Animator.SetTrigger("Die");
        }
        UpdateHpBar();
    }

    public void AddHp(float hp)
    {
        m_Hp += hp;
        if (m_Hp > m_MaxHp)
            m_Hp = m_MaxHp;
        UpdateHpBar();
    }

    void UpdateHpBar()
    {
        m_HpImage.fillAmount = m_Hp / m_MaxHp;
    }

    public bool IsDead()
    {
        return m_Hp <= 0.0f;
    }
}
