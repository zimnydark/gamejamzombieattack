﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Npc
{
    public Npc m_TargetNpc;
    private NavMeshAgent m_NavMeshAgent;
    private GameObject[] m_Fence;
    public float m_AggroDistance;
    private bool m_IsAttacking = false;
    public float m_DeadDissapearTime = 5.0f;
    public AudioClip m_MobHit;

    public override void Start()
    {
        base.Start();

        m_NavMeshAgent = this.GetComponent<NavMeshAgent>();
        m_NavMeshAgent.speed = m_MovementSpeed;
        
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (!m_IsAttacking && !IsDead())
            Move();

        if(IsDead())
        {
            m_DeadDissapearTime -= Time.deltaTime;
            if (m_DeadDissapearTime < 0.0f)
                Destroy(this.gameObject);
        }
    }

    void Move()
    {
        float distance = Vector3.Distance(transform.position, m_TargetNpc.transform.position);
        if (distance < m_AggroDistance / 2 && !m_TargetNpc.IsDead())
        {
            m_IsAttacking = true;
            m_Animator.SetTrigger("Attack");
            m_Animator.SetBool("Idle", false);
            m_Animator.SetBool("Walk", false);
        }
        else if (distance < m_AggroDistance && !m_TargetNpc.IsDead())
        {
            Vector3 directToPlayer = transform.position - m_TargetNpc.transform.position;
            Vector3 newPos = transform.position - directToPlayer;
            m_NavMeshAgent.destination = newPos;
            m_Animator.SetBool("Idle", false);
            m_Animator.SetBool("Walk", true);
        }
        else
        {
            var fences = GameObject.FindGameObjectsWithTag("Fence");
            float closestFenceDistance = Mathf.Infinity;
            GameObject closestFence = null;

            foreach(var fence in fences)
            {
                var fenceDistance = Vector3.Distance(fence.transform.position, transform.position);
                if(fenceDistance < closestFenceDistance)
                {
                    closestFenceDistance = fenceDistance;
                    closestFence = fence;
                }
            }

            m_NavMeshAgent.destination = closestFence.transform.position;

            if (closestFenceDistance < 4)
            {
                m_IsAttacking = true;
                m_Animator.SetTrigger("Attack");
                m_Animator.SetBool("Idle", false);
                m_Animator.SetBool("Walk", false);
            }
            else
            {
                m_Animator.SetBool("Idle", true);
                m_Animator.SetBool("Walk", false);
            }
        }
    }

    void AttackDealDamage_Event()
    {
        Attack();
    }

    void AttackEnd_Event()
    {
        m_IsAttacking = false;
        m_Animator.ResetTrigger("Attack");
    }
}
