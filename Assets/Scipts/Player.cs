﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Npc
{
    public Camera m_Camera;
    public string m_HpRegenTag;
    private bool m_IsAttacking = false;
    public static AudioSource paudio;
    Rigidbody m_RigidBody;

    public override void Start()
    {
        base.Start();

        m_RigidBody = GetComponent<Rigidbody>();
    }

    /*public override void Update()
    {
        if (Input.GetKey(KeyCode.Space) && !m_IsAttacking)
            StartAttack();

        if (!m_IsAttacking && !IsDead())
            Move();
    }*/
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (Input.GetKey(KeyCode.Space) && !m_IsAttacking)
        {
            StartAttack();
        }

        if (!m_IsAttacking && !IsDead())
            Move();

        m_RigidBody.velocity.Set(0.0f, 0.0f, 0.0f);
    }

    void Move()
    {
        // Calculate forward move vector.
        Vector3 forwardDirection = m_Camera.transform.forward;
        forwardDirection.y = 0;
        forwardDirection.Normalize();

        // Calculate side move vector.
        Vector3 sideDirection = Quaternion.Euler(0.0f, 90.0f, 0.0f) * forwardDirection;

        // Move character.
        Vector3 moveDirection = (forwardDirection * Input.GetAxisRaw("Vertical") +
            sideDirection * Input.GetAxisRaw("Horizontal")).normalized;


        Vector3 translation = m_MovementSpeed * moveDirection;
        if (translation.magnitude > 0.000001)
        {
            m_Animator.SetBool("Walk", true);
            m_Animator.SetBool("Idle", false);
            transform.rotation = Quaternion.LookRotation(moveDirection);
            transform.position += translation * Time.deltaTime;

        }
        else
        {
            m_Animator.SetBool("Idle", true);
            m_Animator.SetBool("Walk", false);
        }
    }

    void StartAttack()
    {
        m_IsAttacking = true;
        m_Animator.SetTrigger("Attack");

        m_Animator.SetBool("Idle", false);
        m_Animator.SetBool("Walk", false);
    }

    void AttackDealDamage_Event()
    {
        Attack();
    }

    void AttackEnd_Event()
    {
        m_IsAttacking = false;
        m_Animator.SetBool("Idle", true);
        m_Animator.SetBool("Walk", false);
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag == m_HpRegenTag)
        {
            AddHp(m_HpRegen * Time.deltaTime);
        }
    }
}
