﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public GameObject[] Mobs;
    public float SpawnTime;
    public float SpawnDelay;
   
    float m_xPos;
    float m_zPos;
     float RangeToSpawn=0 ;
    public float WaveMonsters ;
    public BlockSpawner(GameObject[] Mobs, float SpawnTime, float SpawnDelay, float RangeToSpawn, float WaveMonsters)
    {
        this.Mobs = Mobs;
        this.SpawnTime = SpawnTime;
        this.SpawnDelay = SpawnDelay;
        this.RangeToSpawn = RangeToSpawn;
        this.WaveMonsters = WaveMonsters;

    }
    void Start()
    {
        Invoke("SpawnMachine", 0);
        //InvokeRepeating("SpawnMachine", SpawnTime, SpawnDelay);
    }

    void SpawnMachine()
    {
        RandomXZ();
        for (int j = 0; j < WaveMonsters; j++)
        {
            for (int i = 0; i < Mobs.Length; i++)
            {
                float m_newX = Random.Range(m_xPos - RangeToSpawn, m_xPos + RangeToSpawn);
                float m_newZ = Random.Range(m_zPos - RangeToSpawn, m_zPos + RangeToSpawn);
                Instantiate(Mobs[i], new Vector3(m_newX, 0.0f, m_newZ), transform.rotation);
            }
        }
    }
    void RandomXZ()
    {
        int alfa = Random.Range(0, 360);
        float x = Mathf.Sin(alfa);
        float z = Mathf.Cos(alfa);
        float minX = 90 * x;
        float maxX = 100 * x;
        float minZ = 140 * z;
        float maxZ = 150 * z;
        m_xPos = Random.Range(minX, maxX);
        m_zPos = Random.Range(minZ, maxZ);
        transform.position = new Vector3(m_xPos, 0, m_zPos);
    }
}
