﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSpawners : MonoBehaviour
{
    public List<Builder> objectSpawner = new List<Builder>();
    float timer = 0;
    void Update()
    {
        for (int i = 0; i < objectSpawner.Count; i++)
        {
            int alfa = Random.Range(0, 360);
            float x = Mathf.Sin(alfa);
            float z=Mathf.Cos(alfa);
            float minX = 150 * x;
            float maxX = 200 * x;
            float minZ = 175 * z;
            float maxZ = 200 * z;
            if (objectSpawner[i].Timer <= timer)
            {
                objectSpawner[i].myobject.SetActive(true);
                objectSpawner[i].myobject.transform.position =new Vector3 (Random.Range(minX, maxX),0, Random.Range(minZ, maxZ));
            }
            timer += Time.deltaTime;
        }
    }
}
