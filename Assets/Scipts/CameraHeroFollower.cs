﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHeroFollower : MonoBehaviour
{
    private Vector3 m_PositionOffset = new Vector3(0.0f, 0.0f, 0.0f);
    public GameObject m_Hero = null;

    // Start is called before the first frame update
    void Start()
    {
        if (m_Hero)
            m_PositionOffset = transform.position - m_Hero.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = m_Hero.transform.position + m_PositionOffset;
    }
}
