﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Builder
{
    public GameObject myobject;
    public float Timer;


    public Builder(GameObject myobject, float Timer)
    {
        this.myobject = myobject;
        this.Timer = Timer;
    }
}
