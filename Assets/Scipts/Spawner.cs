﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] m_Mobs;
    public float m_SpawnTime = 1;
    public float m_SpawnDelay = 20;
    public bool m_Stop = false;
    float m_xPos;
    float m_zPos;
    public float m_RangeToSpawn=0;
    public float m_WaveOfMonsters = 1;
    void Start()
    {
         m_xPos = transform.position.x;
        m_zPos = transform.position.z;
        //InvokeRepeating("SpawnMachine", m_SpawnTime, m_SpawnDelay);
        Invoke("SpawnMachine",0);
    }

    void SpawnMachine()
    {for (int j = 0; j < m_WaveOfMonsters; j++)
        {
            for (int i = 0; i < m_Mobs.Length; i++)
            {
                float m_newX = Random.Range(m_xPos - m_RangeToSpawn, m_xPos + m_RangeToSpawn);
                float m_newZ = Random.Range(m_zPos - m_RangeToSpawn, m_zPos + m_RangeToSpawn);
                Instantiate(m_Mobs[i], new Vector3(m_newX, 0.5f, m_newZ), transform.rotation);
            }
            if (m_Stop)
            {
                CancelInvoke("SpawnMachine");
            }
        }
    }
    
}
