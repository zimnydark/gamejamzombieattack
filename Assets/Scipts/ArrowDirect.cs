﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowDirect : MonoBehaviour
{
    private GameObject[] m_Mobs;
    public GameObject m_Player;
    float m_Min = 100000;
    Vector3 m_Dir;
    void Update()
    {
        m_Mobs = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i=0; i< m_Mobs.Length;i++)
        {
            if(i==0)
            {
                m_Min = Vector3.Distance(m_Player.transform.position, m_Mobs[i].transform.position);
            }

            if(Vector3.Distance(m_Player.transform.position, m_Mobs[i].transform.position)<=m_Min)
            {
                m_Min = Vector3.Distance(m_Player.transform.position, m_Mobs[i].transform.position);
                    m_Dir = (m_Mobs[i].transform.position - m_Player.transform.position ).normalized;
            }
        }
        this.transform.rotation = Quaternion.LookRotation(m_Dir);
    }
}

